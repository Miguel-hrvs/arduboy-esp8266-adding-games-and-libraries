I recommend using the dev branch

Thanks to Cheungbx, the ESPBoy community and RomanS (who is very kind and helpful). I made this mix of both projects to have a version that features the things that i liked (oled screen), and remove what I didn't (neopixel, tft and mcp). Is begginer friendly too, there's some instructions below.

- ESPboy website: https://www.espboy.com/
- Cheungbx channel: https://www.youtube.com/@BillyCdiy

What works:

- All sound libraries.
- Some games give a blinking led error: Most of them are fixed changing arduboy.boot(); with arduboy.begin();
- Change arduboy.display(true); with arduboy.display();
- FX games work on the dev brach

Arduino 1.18.19 is recommended, newer versions work as well.

Driver to flash the board with arduino (not needed in linux): https://www.wemos.cc/en/latest/ch340_driver.html

Instructions to make a compatible console: https://www.thingiverse.com/thing:6027363

How to set up arduino ide:
1. Install arduino 1.x.x type of version
2. Clone repo as zip and unpack it (7-zip for windows is great)
3. Look where the projects are stored in file -> preferences
4. Copy this on the additional boards section: http://arduino.esp8266.com/stable/package_esp8266com_index.json
5. Copy the separated libraries from the unpacked repo in the folder that you saw. (The GAMES folder can be stored on any folder of your pc).
6. Go to tools -> boards -> board manager and install the esp8266.
7. If the libraries aren't found, move the content from the src folder from each one one upper.
8. Restart arduino
9. Go to tools -> boards and select lolin wemos d1 mini clone
10. Load an .ino file from the GAMES folder
11. Select the com usb port in the tools section.
12. Go to sketch -> verify/compile 
13. Once done sketch -> upload
