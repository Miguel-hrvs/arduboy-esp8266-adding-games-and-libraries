#include "Utils.h"

const __FlashStringHelper * FlashString(const char * string) {
    return reinterpret_cast<const __FlashStringHelper *>(string);
}
