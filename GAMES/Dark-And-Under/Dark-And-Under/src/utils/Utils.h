#ifndef Utils_h
#define Utils_h

#include <WString.h>

const __FlashStringHelper * FlashString(const char * string);

template<typename T> T absT(const T & v) {

	return (v < 0) ? -v : v;

}

#endif // Utils_h
